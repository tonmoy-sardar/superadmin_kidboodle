import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowerrComponent } from './showerr.component';

describe('ShowerrComponent', () => {
  let component: ShowerrComponent;
  let fixture: ComponentFixture<ShowerrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowerrComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowerrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
