import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-showerr',
  templateUrl: './showerr.component.html',
  styleUrls: ['./showerr.component.scss']
})
export class ShowerrComponent implements OnInit {
  @Input()
  errorMsg = ''

  @Input()
  userForm: FormGroup = new FormGroup({});

  @Input()
  userField: string = ''

  constructor() { }

  ngOnInit(): void {
  }

  isFieldValid(form: FormGroup, field: string) {
   
    return !form.get(field)?.valid && (form.get(field)?.dirty || form.get(field)?.touched);
  }

}
