import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { commonResponce } from 'src/app/shared/schoolclass/shared.class';
import { msgduration, msgdurationerr, msghorizontalPosition, msgpanelClass, msgpanelClasserr, msgverticalPosition } from './global';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  loginObj: commonResponce = new commonResponce();
  public showHeaderSubject = new Subject<boolean>();
  public currentYearSubject = new Subject<string>();
  public SubHeaderTitleSubject = new Subject<string>();

  constructor(private snackBar: MatSnackBar,private router: Router) { }



// setCurrentYearSubject() {
//   if (localStorage.getItem("CurrentSession") != "") {
//       this.currentYearSubject.next(localStorage.getItem("CurrentSession"));
//   }
//   else {
//       this.currentYearSubject.next("");
//   }
// }

setshowHeader(settrue: boolean) {
  let logindetails = localStorage.getItem("LoginToken");
  if (logindetails != "")
      this.showHeaderSubject.next(true);
  else
      this.showHeaderSubject.next(false);
}

setSubHeader() { 
  this.SubHeaderTitleSubject.next(
      this.getmenuname(this.router.url.split('/')[1])
  );
}
navigateToroute(route: string) {
  this.router.navigate(['/' + route]);
}

getmenuname(route: string) {
  
  console.log(route);
  switch (route) {
      case 'dashboard': return 'Dashboard';
      case 'holidaymanagement': return 'Holiday Management';
      case 'modulemanagement': return 'Module Management';
      case 'packagemanagement': return 'Package Management';
      case 'schoolmanagement': return 'School Management';

            default: return ''
  }
}

// showHeader() {
//   if (this.loginObj.result.token != "")
//       return true;
// }

showMessage(message: string, action:any, isSuccess:boolean) {
  this.snackBar.open(message, action, {
      duration: 5000,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      //panelClass: ( request_status=='0'?msgpanelClasserr:msgpanelClass),
      panelClass: [isSuccess == true ? 'msg-success': 'msg-error']
  });
}
}
