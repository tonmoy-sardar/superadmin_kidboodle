import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SchoolmanagementService {
  httpHeaderOptions: { headers: HttpHeaders; };
  httpHeaders: any;
headers2:any;
  constructor(private _http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    });
    this.headers2 = new HttpHeaders({
      'Content-Type': 'multipart/form-data',
      'Accept': 'multipart/form-data',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      
    })
    // this.headers2 = new HttpHeaders();
    // this.headers2 = this.headers2.append('Content-Type', 'multipart/form-data');
    // this.headers2 = this.headers2.append('enctype', 'multipart/form-data');
    // this.headers2 = this.headers2.setheaders('Authorization', 'Token ' + localStorage.getItem("LoginToken")+'');
  }

  getSchoolData(params: any): Observable<any> {
    return this._http.get<any[]>(environment.apiURL + "school_crud/", { params: params, headers: this.httpHeaders });
  }

  addSchoolData(body: any): Observable<any> {
    return this._http.post<any[]>(environment.apiURL + "school_crud/", body, this.httpHeaderOptions);
  }

  deleteSchoolData(id: any, body: any): Observable<any> {
    return this._http.put<any[]>(environment.apiURL + "school_crud/?method=delete&school_id="+id, body, { headers: this.httpHeaders });
  }
  updateSchoolStatus(params: any): Observable<any> {
    return this._http.put<any[]>(environment.apiURL + "school_active_or_inactive/?school_id="+params.school_id+"&method=edit&is_active="+params.is_active, params, { headers: this.httpHeaders } );
  }

  updateSchoolDetails(id: any, body: any): Observable<any> {
    return this._http.put<any[]>(environment.apiURL + "school_crud/?method=edit&school_id="+id, body, { headers: this.httpHeaders });
  }

  

}
