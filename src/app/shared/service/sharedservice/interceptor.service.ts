import { Injectable } from '@angular/core';
import {
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
  HttpHandler,
  HttpEvent,
  HttpErrorResponse
} from '@angular/common/http';
import { Observable, Subject, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
// import * as Globals from '../../global'; 

@Injectable({
  providedIn: 'root'
})
export class InterceptorService {

  public SubHeaderTitleSubject = new Subject<string>();

  constructor(
    public router: Router 
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var token: string;
    // if (localStorage.getItem('isLoggedin')) {
    //   token = localStorage.getItem('LoginToken');
    //   request = request.clone({ headers: request.
    //     headers.set('Authorization', 'Token ' + token) }
    //   );
    // }
    
    return next.handle(request).pipe(
      map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse) {
          if (event.body.request_status != "1") {
          //  console.log(event.body.msg)
          }
        }
        return event;
      }),
      catchError((error: HttpErrorResponse) => {
        if (error instanceof HttpErrorResponse) {
          console.log(error);
          if (error.status === 401) {
            // localStorage.clear();
            // window.open(Globals.baseUrl + "#/session-expire", "_self");
            // this.router.navigate(['/login'])
          } else if (error.status === 404) {
            // window.location.reload();
          }
        }
        return throwError(error);
      })
    );




  }
}
