import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HolidaylistService {
  httpHeaderOptions: { headers: HttpHeaders; };
  httpHeaders: any;

  constructor(private _http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        // 'Content-Type': 'application/json',
        // 'Accept': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    })
  }

  getHolidayData(params: any): Observable<any> {
    return this._http.get<any[]>(environment.apiURL + "holiday_leave_list_or_add/", { params: params, headers: this.httpHeaders });
  }

  addHolidayData(body: any): Observable<any> {
    return this._http.post<any[]>(environment.apiURL + "holiday_leave_list_or_add/", body, this.httpHeaderOptions);
  }

  updateHolidayData(id: number, body: any): Observable<any> {
    return this._http.put<any[]>(environment.apiURL + "holiday_leave_edit_or_detail/" + id + "/", body, this.httpHeaders);
  }

  deleteHolidayData(id: any): Observable<any> {
    return this._http.delete<any[]>(environment.apiURL + "holiday_leave_edit_or_detail/" + id + "/", this.httpHeaderOptions);
  }

}
