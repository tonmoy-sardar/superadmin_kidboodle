import { TestBed } from '@angular/core/testing';

import { SchoolmanagementService } from './schoolmanagement.service';

describe('SchoolmanagementService', () => {
  let service: SchoolmanagementService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolmanagementService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
