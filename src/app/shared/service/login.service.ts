import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  httpHeaderOptions: any;
  authHeaders;

  constructor(private _http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }

    this.authHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      
    })
  }

  sessionKey: string = "u234zx90we#$";
  userNameKey: string = "u234u23n99a";

  // GetSessionID(): string {
  //     if (localStorage.getItem(this.EncodeString(this.sessionKey)) != null)
  //         return this.DecodeString(localStorage.getItem(this.EncodeString(this.sessionKey)));
  //     else
  //         return "";
  // }

  // GetUserName(): string {
  //     if (localStorage.getItem(this.EncodeString(this.userNameKey)) != null)
  //         return this.DecodeString(localStorage.getItem(this.EncodeString(this.userNameKey)));
  //     else
  //         return "";
  // }

  ClearSession(): void {
      localStorage.removeItem(this.EncodeString(this.sessionKey));
      localStorage.removeItem(this.EncodeString(this.userNameKey));
  }

  SetSession(sessionID: string, userName: string): void {
      localStorage.setItem(this.EncodeString(this.sessionKey), this.EncodeString(sessionID));
      localStorage.setItem(this.EncodeString(this.userNameKey), this.EncodeString(userName));
  }

//   IsSessionActive(): boolean {
//       return ((this.GetSessionID() != "" && this.GetUserName() != ""));
// }

  private EncodeString(val: string): string {
      return window.btoa(val);
  }

  private DecodeString(val: string): string {
      return window.atob(val);
  }


  getLoginData(Username:string, Password: string): Observable<any> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', '*');

    return this._http.post<any[]>(environment.apiURL+ "login/", {
      password: Password,
      username:Username,
      auth_provider: 'admin'
    })
  }

  GetAdminDetails(): Observable<any> {
       return this._http.get<any[]>( environment.apiURL  + "school_admin_details/", this.httpHeaderOptions)
  }

  getCnfpswdData(params:any ): Observable<any> {
    return this._http.post<any[]>( environment.apiURL + "forgot_password/",params, {headers:this.authHeaders})
  }
  getphone_number_verify(params:any ): Observable<any> {
    return this._http.post<any[]>( environment.apiURL + "phone_number_verify/",params, {headers:this.authHeaders })
  }
  getemail_verify(params:any ): Observable<any> {
    return this._http.post<any[]>( environment.apiURL + "email_verify/",params, {headers:this.authHeaders })
  }
  getemail_exist(body:any):Observable<any>{
    return this._http.post<any[]>( environment.apiURL + "email_exists_or_not/",body, {headers:this.authHeaders})
  }

}
