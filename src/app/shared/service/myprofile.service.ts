import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MyprofileService {
  httpHeaders: HttpHeaders;
  httpHeaderOptions: { headers: HttpHeaders; };

  constructor(private _http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders =  new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    })
  }

 
  getMyprofileData(old_password: string, new_password: string): Observable<any> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('Access-Control-Allow-Origin', '*');

    return this._http.put<any[]>(environment.apiURL + "change_password/", {
      old_password: old_password,
      new_password: new_password,
      auth_provider: 'school_user',
    });
  }
}