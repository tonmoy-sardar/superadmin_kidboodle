import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PackageService {
  httpHeaderOptions: any;
  httpHeaders: HttpHeaders;
  APIUrl: string = environment.apiURL;
  constructor(private _http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    })
   }
   
  GetPanelType(params :any): Observable<any> {

    return this._http.get<any[]>(this.APIUrl + "superadmin/module_type_list_admin/", { params: params, headers: this.httpHeaders });

  }
  GetAllPackage(params :any): Observable<any> {

    return this._http.get<any[]>(this.APIUrl + "superadmin/package_add_or_list/", { params: params, headers: this.httpHeaders });

  }
  
  DeletePackage(id:number): Observable<any> {

    return this._http.delete<any[]>(this.APIUrl + "superadmin/package_edit_detail_delete/"+id+"/", { headers: this.httpHeaders });

  }

  UpdatePackage(id:number): Observable<any> {

    return this._http.put<any[]>(this.APIUrl + "superadmin/package_edit_detail_delete/"+id+"/", { headers: this.httpHeaders });

  }

  Packagedetails(params:any): Observable<any> {

    return this._http.get<any[]>(this.APIUrl + "superadmin/module_type_list_admin_packages/", { params: params, headers: this.httpHeaders });

  }
  
  postpackage(body :any): Observable<any> {

    return this._http.post<any[]>(this.APIUrl + "superadmin/package_add_or_list/", body,  {headers:this.httpHeaders});

  }

  SearchPackagedetails(params:any): Observable<any> {

    return this._http.get<any[]>(this.APIUrl + "superadmin/package_search/", { params: params, headers: this.httpHeaders });

  }

}
