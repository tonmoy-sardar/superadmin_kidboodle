import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  httpHeaderOptions: { headers: HttpHeaders; };
  httpHeaders: any;
headers2:any;
  constructor(private _http: HttpClient) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    });
    this.headers2 = new HttpHeaders({
      'Content-Type': 'multipart/form-data',
      'Accept': 'multipart/form-data',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      
    })
   
  }

  GetAllStudentsDetails(params:any): Observable<any> {
    return this._http.get<any[]>(environment.apiURL + "school_student_list/", { params: params });
  }

  GetAllSchoolTeachers(params:any): Observable<any> {
    return this._http.get<any[]>(environment.apiURL + "school_user_detail_list/", { params: params, headers: this.httpHeaders });
  }
  
  GetAllPackage(params:any): Observable<any> {
    return this._http.get<any[]>(environment.apiURL + "superadmin/package_add_or_list/", { params: params, headers: this.httpHeaders });
  }

  getSchoolData(params:any): Observable<any> {
    return this._http.get<any[]>(environment.apiURL + "school_crud/", { params: params, headers: this.httpHeaders });
  }

  GetAllstudteach(params:any): Observable<any> {
    return this._http.get<any[]>(environment.apiURL + "superadmin/teacher_student_count/", { params: params, headers: this.httpHeaders });
  }


}