import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  httpHeaderOptions: any;
  httpHeaders: HttpHeaders;
  APIUrl: string = environment.apiURL;

  constructor(
    private _http: HttpClient
  ) {
    this.httpHeaderOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Token ' + localStorage.getItem("LoginToken")
      })
    }
    this.httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Token ' + localStorage.getItem("LoginToken")
    })
  }

  GetPanelType(params :any): Observable<any> {
    return this._http.get<any[]>(this.APIUrl + "superadmin/module_type_list/", { params: params, headers: this.httpHeaders });
  }
  GetModuleList(params :any): Observable<any> {
    return this._http.get<any[]>(this.APIUrl + "superadmin/module_list/", { params: params, headers: this.httpHeaders });
  }
  PostModuleList(body:any ): Observable<any> {
     return this._http.post<any[]>(this.APIUrl + "superadmin/module_add/", body, this.httpHeaderOptions);
    }
  DeleteModuleList(id:any ): Observable<any> {
    return this._http.delete<any[]>(this.APIUrl + "superadmin/module_edit_or_detail/"+id+"/", this.httpHeaderOptions);
  }

 changePassword(id:any,body:any):Observable<any>{
   return this._http.put<any[]>(this.APIUrl + "school_user_edit_login_creds/"+id+"/",body,this.httpHeaderOptions);
   }

}
