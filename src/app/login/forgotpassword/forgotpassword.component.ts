import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition } from 'src/app/shared/global';
import { SharedService } from 'src/app/shared/shared.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { LoginService } from 'src/app/shared/service/login.service';


@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {

  email: string = "";
  mobile: string = "";
  emailotp : string="";
  mobileotp : string="";
  isOtpCorrect = false;
  isOtp:boolean=false;
  isMobileOtp :boolean=false;
  new_password : string="";
  confirm_password : string="";
  MobilegetOtp:any;
  EmailgetOtp:any;
  forClosingEditModal:any;
  isOtpMsg:any;
  userName:any;

  closeResult = '';

  @ViewChild('myModal') myModal: TemplateRef<any> | undefined;
  @ViewChild('editModal') editModal: TemplateRef<any> | undefined;


  constructor(private _loginService: LoginService, private _sharedService: SharedService,
     private _snackBar: MatSnackBar, private modalService: NgbModal) { }

  ngOnInit(): void {
    localStorage.clear();
  }

  getCnfpswdData() {

  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


  requestNewPass() {

    if (this.mobile == "" && this.email == "") {
      this._sharedService.showMessage("Enter either mobile no or email id." , "X",false)
      return;
      
    }

    if (this.mobile != "") {
      let params = {
        dial_code: "+91",
        phone_no: this.mobile,
      }

      this._loginService.getphone_number_verify(params).subscribe((response) => {
        console.log(response);

        if (response.request_status == 1) {
          this.modalService.open(this.editModal);
          this.isOtpMsg = response.msg;
          this.isOtp = true;
          this.MobilegetOtp=response.results.otp;
          this.isMobileOtp = true;
          console.log(this.MobilegetOtp);
        }
        else {
          alert(response.msg);
        }
      }, err => {
        console.log(err);
      });
      return;
    }

    
    if (this.email != "") {
      let body = {
        email_id: this.email,
        auth_provider : 'admin'
      }
      this._loginService.getemail_exist(body).subscribe((res)=>{
        console.log(res);
          this._sharedService.showMessage("This Email Id is not Exist!", "X",false);
      }, err => {
        if(err.status==409){

        let params ={
          email_id:this.email
        }
         this._loginService.getemail_verify(params).subscribe((response) => {
           console.log(response);
           if (response.request_status == 1) {
             this.modalService.open(this.editModal);
             this.isOtpMsg = response.msg;
             this.isOtp = true;
             this.EmailgetOtp=response.results.otp;
           }
         });
        }
        
      })
     
    }
  }

  otpSubmit(){
    this.modalService.dismissAll();
    if (this.mobileotp == "" && this.emailotp == "") {
      this._sharedService.showMessage("Please Enter The OTP!", "X",false)
      return;
    }

    if (this.mobileotp != "") {
      let mobotp: string = atob(this.MobilegetOtp);

      if (this.mobileotp!== mobotp) {
        this._sharedService.showMessage("Please Enter Correct OTP!", "X",false)
        return;
      }
      this.isOtpCorrect = true;
      this.modalService.open(this.myModal);
    }
    if (this.emailotp!= "") {
      let emaotp: string = atob(this.EmailgetOtp);
      if (this.emailotp!== emaotp) {
        alert("please enter correct otp!");
        return;
      }
      this.isOtpCorrect = true;
      this.modalService.open(this.myModal);
    }
  
    }

  btnChnagepassword_click() {

    if (this.new_password == this.confirm_password) {
      
    let params = {
      email: this.email,
      phone: this.mobile,
      new_password:this.new_password,
      confirm_password:this.confirm_password,
      auth_provider: 'admin',
      username :this.userName
    }

    this._loginService.getCnfpswdData(params).subscribe((response) => {
      console.log(response);
      if (response.request_status == 1) {
        this._sharedService.showMessage("Password Change Successfully", "X",true);
        this.modalService.dismissAll();
      }
      else{
        this._sharedService.showMessage("Matching User does not Exist !", "X",false);
      }
    }, err => {
      this._sharedService.showMessage("Your new password is similar to old password. Please try with another password.",  "X",false);
    });
  }
  else{
    this._sharedService.showMessage("New Password and Confirm Password Should be Same" , "X",false);
    this.modalService.open(this.myModal);
  }
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

}
