import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginClass } from 'src/app/shared/schoolclass/loginpage.class';
import { MatSnackBar } from '@angular/material/snack-bar';
import { commonResponce } from 'src/app/shared/schoolclass/shared.class';
import { SharedService } from 'src/app/shared/shared.service'; 
import { LoginService } from 'src/app/shared/service/login.service'; 

@Component({
  selector: 'app-loginpage',
  templateUrl: './loginpage.component.html',
  styleUrls: ['./loginpage.component.scss']
})
export class LoginpageComponent implements OnInit {
  fieldTextType: boolean | undefined;


  loginpage: LoginClass = new LoginClass();
  loginresponce: commonResponce = new commonResponce();

  constructor(private router: Router, private _loginService: LoginService, private _sharedService: SharedService, private _snackBar: MatSnackBar) {
    this._sharedService.setshowHeader(false);
   }

  ngOnInit(): void {
  }

  goToForgotPassword_click() {
    this.router.navigate(['/forgotpassword']);
  }

  // btnLogin_click() {
  //   this.router.navigate(['/dashboard']);
  // }

  Login_click() {
    console.log(localStorage.getItem('isLoggedin'))

    if (this.loginpage.password == "" || this.loginpage.username == "") {
      this._sharedService.showMessage("please enter username and password",  "X",false)
      return
    }
    this._loginService.getLoginData(this.loginpage.username, this.loginpage.password).subscribe((response) => {
      this.loginresponce = response;
      console.log(response)
      if (response.request_status == "1") {
        this._sharedService.loginObj = this.loginresponce;
        if (this.loginresponce?.result?.token != "") {
          localStorage.setItem("LoginToken", this.loginresponce?.result?.token);
          //localStorage.setItem("CurrentSession", response.result.user_details.session.id);
          localStorage.setItem("isLoggedin", "true");
          // this._sharedService.setCurrentYearSubject();

          let username = response.result.user_details.first_name + " " + response.result.user_details.last_name
          localStorage.setItem("username", username);
          localStorage.setItem("user_id", response.result.user_details.user_id);
          localStorage.setItem("school_user_id", response.result.user_details.school_user_id);
          localStorage.setItem("email_id", response.result.user_details.email);

          switch (this.loginresponce?.result?.user_details?.roll) {
            case 1:
              // this.router.navigate(['/dashboard']);
              this.router.navigate(['/dashboard'])
              .then(() => {
               window.location.reload();
                });
              break;
            default:
              // this.router.navigate(['/login']);
          }
        }
        else {
          this._sharedService.showMessage("Username or password is incorrect!", "X",false)
        }
        this._sharedService.setshowHeader(false);
      }
      else {
        this._sharedService.showMessage("Please enter valid username or password", "X",false)
      }
    },
      error => {
        console.log(error);
        this._sharedService.showMessage("please enter valid username or password.", "X",false);
      }
    );

  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  
}
