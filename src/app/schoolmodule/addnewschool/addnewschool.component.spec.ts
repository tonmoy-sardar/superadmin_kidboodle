import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnewschoolComponent } from './addnewschool.component';

describe('AddnewschoolComponent', () => {
  let component: AddnewschoolComponent;
  let fixture: ComponentFixture<AddnewschoolComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddnewschoolComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnewschoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
