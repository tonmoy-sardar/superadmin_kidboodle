import { Location } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar,MatSnackBarHorizontalPosition,MatSnackBarVerticalPosition, } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition } from 'src/app/shared/global';
import { DataService } from 'src/app/shared/service/data.service';
import { PackageService } from 'src/app/shared/service/package.service';
import { SchoolmanagementService } from 'src/app/shared/service/schoolmanagement.service';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-addnewschool',
  templateUrl: './addnewschool.component.html',
  styleUrls: ['./addnewschool.component.scss']
})
export class AddnewschoolComponent implements OnInit {
  fieldTextType: boolean | undefined;

  


  @ViewChild('editModal') editModal: TemplateRef<any> | undefined;

  packageData: any;
  schoolForm: FormGroup;
  SchoolList: any;

  schoolname: string = '';
  address1: string = '';
  address2: string = '';
  package: string = '';
  doj: string = '';
  reg_no: string = '';
  unique_id: string = '';
  pin_code: string = '';
  board: string = '';
  dial_code: string = '';
  contact_person_name: string = '';
  contact_person_phone: string = '';
  contact_person_email: string = '';
  school_phone1: string = '';
  school_phone2: string = '';
  school_email: string = '';
  account_holder: string = '';
  branch_name: string = '';
  bank_name: string = '';
  ifsc_code: string = '';
  account_number: string = '';
  is_active: boolean = true;
  attendence_type: string = '';
  country_code: string = '';
  state_code: string = '';
  city_code: string = '';
  username: string = '';
  password: string = '';
  schoollogo="Choose file";
  horizontalPosition: MatSnackBarHorizontalPosition = 'start';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  constructor(private _location: Location, private formBuilder: FormBuilder,
    private packageservice: PackageService,
    private dataService:DataService,
    private _snackBar: MatSnackBar, private modalService: NgbModal, private _router: Router, private schoolManagement: SchoolmanagementService, private _sharedService: SharedService) {
    this.schoolForm = this.formBuilder.group({
      schoolname: ['', [Validators.required]],
      address1: ['', [Validators.required]],
      doj: ['', [Validators.required]],
      package: ['', [Validators.required]],
      school_logo: ['', Validators.required],
      contact_person_name: ['', [Validators.required]],
      contact_person_phone: ['', [Validators.required]],
      contact_person_email: ['', [Validators.required, Validators.email]],
      account_holder: ['', [Validators.required]],
      bank_name: ['', [Validators.required]],
      country_code: ['', [Validators.required]],
      ifsc_code: ['', [Validators.required]],
      account_number: ['', [Validators.required]],
      is_active: [''],
      username: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
    })
  }


  ngOnInit(): void {
    this.getData();
  }


  getData() {

    let params = {
      page_size: '0',

    }
    this.packageservice.GetAllPackage(params).subscribe(
      responce => {
        console.log(responce);
        this.packageData = responce;
      });
  }
  selectFile(event:any) {

    if (event.target.files[0].type == "image/png" || event.target.files[0].type == "image/jpeg" || event.target.files[0].type == "image/jpg") {
      if (event.target.files && event.target.files[0]) {
        var filesAmount = event.target.files.length;
        for (let i = 0; i < filesAmount; i++) {
          var reader = new FileReader();
          reader.onload = (event) => {
            //console.log(reader.result);
          }
          reader.readAsDataURL(event.target.files[i]);

          
           this.schoolForm.patchValue({
            school_logo: event.target.files[0]
            })
            this.schoollogo = event.target.files[0].name;
          
        }
      }
    } else {

      this.schoollogo = "Choose file";
      alert("Please select png / jpeg / jpg");
    }
  }

  // applyCss(form: FormGroup, field: string) {
  //   return this.displayFieldCss(form, field);
  // }
  // applyValidation(form: FormGroup, field: string) {
  //   return this.isFieldValid(form, field);
  // }

  addDetails() {
    var formValue = new FormData();
    formValue.append('school_logo', this.schoolForm.value.school_logo);
    if (this.schoolForm.value.school_logo) {
      formValue.append('school_logo', this.schoolForm.value.school_logo, this.schoolForm.value.school_logo['name']);
    }
    else {
      formValue.append('school_logo', '');
    }
    if (this.schoolForm.valid) {
    var data= {
        school_name: this.schoolname,
        address1: this.address1,
        address2: this.address2,
        package: this.schoolForm.value.package,
        doj: this.doj,
        reg_no: this.reg_no,
        unique_id: this.unique_id,
        pin_code: this.pin_code,
        board: this.board,
        dial_code: this.dial_code,
        contact_person_name: this.contact_person_name,
        contact_person_phone: this.contact_person_phone,
        contact_person_email: this.contact_person_email,
        school_phone1: this.school_phone1,
        school_phone2: this.school_phone2,
        school_email: this.school_email,
        account_holder: this.account_holder,
        branch_name: this.branch_name,
        bank_name: this.bank_name,
        ifsc_code: this.ifsc_code,
        account_number: this.account_number,
        is_active: this.is_active,
        attendence_type: this.attendence_type,
        country_code: this.country_code,
        state_code: this.state_code,
        city_code: this.city_code,
        username: this.username,
        password: this.password
    }
    let body = JSON.stringify(data);
    console.log(body);

    formValue.set('data', body);
    this.schoolManagement.addSchoolData(formValue).subscribe((response) => {
      localStorage.setItem('msg','One Record Added Successfully!');  
      this._router.navigate(['/schoolmanagement']);
      console.log(response);
    }, err => {
      if(err.status==400){
        this._sharedService.showMessage("Username Already Exists!", "X",false);
      }
      else {
        this._router.navigate(['/schoolmanagement']);
      }
    });
    
  }
  else {
    this.markFormGroupTouched(this.schoolForm);
    console.log(this.schoolForm);
  }
  }

  addSchoolDetails() {
    this._router.navigate(['/schoolmanagement']);
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

  displayStyle = "none";
  closePopup() {
    this.displayStyle = "none";
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: { markAsTouched: () => void; controls: any[]; })=> {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach((c: FormGroup) => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched);
  }
  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field)!.invalid && (form.get(field)!.dirty || form.get(field)!.touched),
      'is-valid': form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched)
    };
  }

  goBack(): void {
    this._location.back();
  }

  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

}
