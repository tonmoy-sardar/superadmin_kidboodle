import { Location } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { itemPerPageNumList, msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition, page_size } from 'src/app/shared/global';
import { SchoolmanagementService } from 'src/app/shared/service/schoolmanagement.service';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-viewschooldetails',
  templateUrl: './viewschooldetails.component.html',
  styleUrls: ['./viewschooldetails.component.scss']
})
export class ViewschooldetailsComponent implements OnInit {
  @ViewChild('editModal') editModal: TemplateRef<any> | undefined;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  pageEvent!: PageEvent;
  pageSize: string = page_size;
  pageSizeOption: Array<number> = itemPerPageNumList;
  pageNo: string = "1";
  totalItems: number = 0;

  userForm: any;
  // SchoolList: any;
  SchoolList = {
    "schoolname": "",
    "address1": "",
    "address2": "",
    "package": "",
    "doj": "",
    "reg_no": "",
    "unique_id": "",
    "pin_code": "",
    "board": "",
    "dial_code": "",
    "contact_person_name": "",
    "contact_person_phone": "",
    "contact_person_email": "",
    "school_phone1": "",
    "school_phone2": "",
    "school_email": "",
    "account_holder": "",
    "branch_name": "",
    "bank_name": "",
    "ifsc_code": "",
    "account_number": "",
    "is_active": "",
    "attendence_type": "",
    "country_code": "",
    "state_code": "",
    "city_code": "",
    "username": "",
    "password": "",
  }


  constructor(private _location: Location, private _snackBar: MatSnackBar, private modalService: NgbModal, private router: Router, private schoolManagement: SchoolmanagementService, private _sharedService: SharedService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    var params = {
      page_size: this.pageSize,

    }
    this.schoolManagement.getSchoolData(params).subscribe((responce) => {
      console.log(responce);
      this.SchoolList = responce.results;
    })
  }

  addSchoolDetails() {
    this.router.navigate(['/schoolmanagement']);
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

  displayStyle = "none";
  closePopup() {
    this.displayStyle = "none";
  }

  goBack(): void {
    this._location.back();
  }

}