import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewschooldetailsComponent } from './viewschooldetails.component';

describe('ViewschooldetailsComponent', () => {
  let component: ViewschooldetailsComponent;
  let fixture: ComponentFixture<ViewschooldetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewschooldetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewschooldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
