import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { itemPerPageNumList, msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition, page_size } from 'src/app/shared/global';
import { SharedService } from 'src/app/shared/shared.service';
import { Location } from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModuleService } from 'src/app/shared/service/module.service';
import { Router } from '@angular/router';
import { ngxCsv } from 'ngx-csv/ngx-csv';


@Component({
  selector: 'app-modulemanagement',
  templateUrl: './modulemanagement.component.html',
  styleUrls: ['./modulemanagement.component.scss']
})
export class ModulemanagementComponent implements OnInit {

  @ViewChild('editModal') editModal: TemplateRef<any> | undefined;

  sub: Subscription = new Subscription();

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  pageEvent!: PageEvent;
  PanelId: string = '';

  moduleForm!: FormGroup;

  pageSize:number = 10;

  // pageSize: string = page_size;
  pageSizeOption: Array<number> = itemPerPageNumList;
  pageNo: string = "1";
  page: number = 1;

  totalItems: number = 0;
  p: number = 1;
  panelData: any;
  moduleData: any;
  pannel_type: string = '';
  parent_id: string = '0';
  name: string = '';
  ModuleValue: any = [];



  allClassDetails: any;
  constructor(
    private formBuilder: FormBuilder, private _snackBar: MatSnackBar,
    private _sharedService: SharedService, private router: Router,
    private _location: Location, private modalService: NgbModal,
    private _module: ModuleService
  ) {
    this.moduleForm = formBuilder.group({
      pannel_type: ['', Validators.required],
      name: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.getData();
    this.getPanelType();
  }


  getData() {
    var params = {
      page_size: 0
    }
    this._module.GetModuleList(params).subscribe(res => {
      console.log(res);
      this.totalItems = res.count;
      this.moduleData = res;
      //this.childData=res.child_modules.name;
    })

  }

  onSearch() {
    var params = {
      page_size: 100,
      module_type: this.PanelId
    }
    this._module.GetModuleList(params).subscribe(res => {
      console.log(res);
      this.moduleData = res.results;
    })

  }

  getPanelType() {
    var params = {
      page_size: 0
    }
    this._module.GetPanelType(params).subscribe(data => {
      console.log(data.httpStatus);
      console.log(data);
      this.panelData = data;
    })

  }

  addModel() {
    if (this.moduleForm.valid) {
      var body = {
        name: this.name,
        module_type: this.pannel_type,
        parent_id: this.parent_id
      }
      this._module.PostModuleList(body).subscribe(response => {
        console.log(response);
        this._sharedService.showMessage("Record Added successfully!", "X", true);
        this.modalService.dismissAll();
        this.moduleForm.reset();
        this.pannel_type = '';
        this.name = '';
        this.getData();
      }
        , err => {
          if (err.status == 400) {
            this._sharedService.showMessage("Module Already Exists!", "X", false);
          }
        }
      );
    }
    else {

      this.markFormGroupTouched(this.moduleForm);
      // this._dataService.markFormGroupTouched(this.userForm);
    }
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  openModel() {
    this.modalService.open(this.editModal, { size: 'lg' });
  }

  displayStyle = "none";
  closePopup() {
    this.displayStyle = "none";
  }

  // onChangePage(pe: PageEvent) {
  //   this.pageSize = pe.pageSize.toString();
  //   this.pageNo = (pe.pageIndex + 1).toString();
  //   this.getData()
  // }

  goBack(): void {
    this._location.back();
  }

  btnDelete_click(id: any) {
    var del = confirm('Are You want To Delete?')
    if (del) {
      this._sharedService.showMessage("Record Deleted Successfully!", "X", false);
      this._module.DeleteModuleList(id).subscribe((responce) => {


      })
      setTimeout(() => {
        var params = {
          page_size: 0
        }
        this._module.GetModuleList(params).subscribe(res => {
          console.log(res);
          this.totalItems = res.count;
          this.moduleData = res;
          //this.childData=res.child_modules.name;
        })
      }, 4000);
    }

  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: { markAsTouched: () => void; controls: any[]; }) => {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach((c: FormGroup) => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched);
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field)!.invalid && (form.get(field)!.dirty || form.get(field)!.touched),
      'is-valid': form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched)
    };
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

  handlePageChange(event:any){ 
    this.page = event;
}

FormDownload(){


  for (const item of this.moduleData){
    var obj = {
      pannel_type: item.pannel_type,
      name: item.name,
    }
    this.ModuleValue.push(obj);
  }


  var options = { 
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true, 
    showTitle: false,
    title: 'moduleData',
    useBom: true,
    headers: ["Panel Type", "Module Name"]
  };
 
  new ngxCsv(this.ModuleValue,"moduleData", options);
}

}
