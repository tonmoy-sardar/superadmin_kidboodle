import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditschooldetailsComponent } from './editschooldetails.component';

describe('EditschooldetailsComponent', () => {
  let component: EditschooldetailsComponent;
  let fixture: ComponentFixture<EditschooldetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditschooldetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditschooldetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
