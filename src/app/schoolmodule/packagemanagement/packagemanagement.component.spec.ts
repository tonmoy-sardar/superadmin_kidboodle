import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PackagemanagementComponent } from './packagemanagement.component';

describe('PackagemanagementComponent', () => {
  let component: PackagemanagementComponent;
  let fixture: ComponentFixture<PackagemanagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PackagemanagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PackagemanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
