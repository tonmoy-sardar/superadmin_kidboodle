import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { itemPerPageNumList, msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition, page_size } from 'src/app/shared/global';
import { SharedService } from 'src/app/shared/shared.service';
import {Location} from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { PackageService } from '../../shared/service/package.service';
import { ModuleService } from 'src/app/shared/service/module.service';
import { ngxCsv } from 'ngx-csv/ngx-csv';



@Component({
  selector: 'app-packagemanagement',
  templateUrl: './packagemanagement.component.html',
  styleUrls: ['./packagemanagement.component.scss']
})
export class PackagemanagementComponent implements OnInit {

  @ViewChild('editModal') editModal: TemplateRef<any> | undefined  ;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  pageEvent!: PageEvent;
  pageSize:number = 10;
  pageSizeOption: Array<number> = itemPerPageNumList;
  pageNo: string = "1";
  totalItems: number = 0;
  page: number = 1;
  p: number = 1;
  allpackage:any;
  packageForm:any;
  singlePackageData:any=[];
  packagename: string = '';
  PackageData: any = [];
  private module: string[] = [];
  PackageListDropdown:any;
  packageid:string = '';
  TeacherModuleData:any=[];
  ModuleData:any=[];
  StudentModuleData:any=[];
  SchoolAdminData:any=[];

  constructor(private _sharedservice: SharedService, private packageservice:PackageService,private _location:Location , private router:Router, private modalService:NgbModal) { }

  ngOnInit(): void {
    this.get_package();
  }
  delete_package(id:number){
    var del = confirm('Are You want To Delete?')
    if(del){
    this.packageservice.DeletePackage(id).subscribe(
      responce => {
        this._sharedservice.showMessage("Record Deleted Successfully!" , "X",false);
        this.get_package();
        
      }
    );
    setTimeout(() =>{ 
      this._sharedservice.showMessage("Record Deleted Successfully!" , "X",false);
        this.get_package();
    }, 4000);
  }
}
get_package(){
  let params = {
    page_size: 100,
    // package_id: this.package_id
  }
  this.packageservice.GetAllPackage(params).subscribe(
    responce => {
      console.log(responce);
      this.totalItems = responce.length;
        this.allpackage = responce.results;
    }
  )
}



onSearch(){
  var params = {
    page_size: this.pageSize,
    package_id: this.packageid
  }
  this.packageservice.SearchPackagedetails(params).subscribe((responce) => {
    console.log(responce);
    this.allpackage = responce.results;
  })
}

  openDetails(id: any){
    console.log(id);
    localStorage.setItem('package_id',id);
    this.router.navigate(['/packagedetails']); 
  }

  btnBack_click(){
    this.router.navigate(['/addpackage']); 
  }

  goBack(): void {
    this._location.back();
  }

  viewSchool(id:any) {
    // this.router.navigate(['/viewschooldetails']);
    this.modalService.open(this.editModal, { size: 'lg' });
    // let packageData = this.allpackage.find((e: { id: any; }) =>e.id == id);
    // if(packageData !== null){
    //   this.singlePackageData = packageData;
    //   console.log(this.singlePackageData);
    // }
    let params = {
      page_size: 100,
      package_id:id
    }
    this.packageservice.Packagedetails(params).subscribe(
      responce => {
        console.log(responce);
          this.singlePackageData = responce;
          this.ModuleData=responce[0].modules;
          this.TeacherModuleData = this.ModuleData.find((e: { name: string; }) => e.name == 'Teacher Module').module_datas;
          this.StudentModuleData = this.ModuleData.find((e: { name: string; }) => e.name == 'Student Module').module_datas;
          this.SchoolAdminData = this.ModuleData.find((e: { name: string; }) => e.name == 'SchoolAdmin Module').module_datas;
          console.log(this.ModuleData)
      }
    )
  }

  editPackage(id:any){
    console.log(id);
    localStorage.setItem('package_id',id);
    this.router.navigate(['/editpackagedetails']);
  }
  
  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: { markAsTouched: () => void; controls: any[]; })=> {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach((c: FormGroup) => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched);
  }
  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field)!.invalid && (form.get(field)!.dirty || form.get(field)!.touched),
      'is-valid': form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched)
    };
  }
  handlePageChange(event:any){ 
    this.page = event;
} 

FormDownload(){


  for (const item of this.allpackage){
    var obj = {
      name:item.name,
      license_key:item.license_key,
      price:item.price,
      total_amount: item.total_amount,
      duration_in_month:item.duration_in_month,

    }
    this.PackageData.push(obj);
  }


  var options = { 
    fieldSeparator: ',',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true, 
    showTitle: true,
    title: 'allpackage',
    useBom: true,
    headers: ["Package Name", "License Key", "Price", "Total Amount", "Duration ( In Month )"]
  };
 
  new ngxCsv(this.PackageData,"allpackage", options);
}

}
