import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PackageService } from '../../shared/service/package.service';
import { msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition } from 'src/app/shared/global';
import { SharedService } from 'src/app/shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addpackage',
  templateUrl: './addpackage.component.html',
  styleUrls: ['./addpackage.component.scss']
})
export class AddpackageComponent implements OnInit {
  packageForm: FormGroup;
  allModule = new Array();
  TeacherModuleData:any=[];
  StudentModuleData:any=[];
  SchoolAdminData:any=[];

  durationInSeconds = 5;
  private module: string[] = [];
  constructor(private packageservice: PackageService,
    private _sharedService: SharedService, private _location: Location, private _snackBar: MatSnackBar, private formBuilder: FormBuilder, private _router: Router) {
    this.packageForm = formBuilder.group({
      name: ['', Validators.required],
      license_key: ['', Validators.required],
      price: ['', Validators.required],
      duration_in_month: ['', Validators.required],
      module: ['', Validators.required],
    })
  }

  ngOnInit(): void {
    this.getData();
  }
  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: { markAsTouched: () => void; controls: any[]; }) => {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach((c: FormGroup) => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched);
  }

  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field)!.invalid && (form.get(field)!.dirty || form.get(field)!.touched),
      'is-valid': form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched)
    };
  }

  module_insert(e: string) {

    const index: number = this.module.indexOf(e);
    if (index !== -1) {
      this.module.splice(index, 1);
    } else {
      this.module.push(e);
    }
    console.log(this.module);
  }
  
  btnSave_click() {
    console.log(this.packageForm.value);
    if (this.packageForm.valid) {
      let body = {
        name: this.packageForm.get('name')!.value,
        license_key: this.packageForm.get('license_key')!.value,
        price: this.packageForm.get('price')!.value,
        duration_in_month: this.packageForm.get('duration_in_month')!.value,
        modules: this.module
      }
      this.packageservice.postpackage(body).subscribe(
        responce => {
          console.log(responce);
          this._sharedService.showMessage('Package Added Successfully', "X", true);
          this._router.navigate(['/packagemanagement']);
          this.packageForm.reset();

        }
      )
      console.log(body);
    } else {
      this.markFormGroupTouched(this.packageForm);
    }
  }

  getData() {
    let params = {
      page_size: 100,
    }

    this.packageservice.GetPanelType(params).subscribe(
      responce => {
        // if (responce.count > 0) {
          this.allModule = responce;
         this.TeacherModuleData = this.allModule.find(e => e.name == 'Teacher Module').module_datas;
         this.StudentModuleData = this.allModule.find(e => e.name == 'Student Module').module_datas;
         this.SchoolAdminData = this.allModule.find(e => e.name == 'SchoolAdmin Module').module_datas;
          console.log(this.allModule);
          // console.log(this.TeacherModuleData);
        }
      // }
    )
  }

  goBack(): void {
    this._location.back();
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

}
