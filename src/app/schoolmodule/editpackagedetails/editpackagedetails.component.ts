import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { itemPerPageNumList, msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition, page_size } from 'src/app/shared/global';
import { SharedService } from 'src/app/shared/shared.service';
import {Location} from '@angular/common';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { PackageService } from '../../shared/service/package.service';
import { ModuleService } from 'src/app/shared/service/module.service';

@Component({
  selector: 'app-editpackagedetails',
  templateUrl: './editpackagedetails.component.html',
  styleUrls: ['./editpackagedetails.component.scss']
})
export class EditpackagedetailsComponent implements OnInit {
  @ViewChild('editModal') editModal: TemplateRef<any> | undefined  ;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  pageEvent!: PageEvent;
  pageSize: string = page_size;
  pageSizeOption: Array<number> = itemPerPageNumList;
  pageNo: string = "1";
  totalItems: number = 0;
  p: number = 1;
  allpackage:any;
  packageForm: FormGroup;
  allModule = new Array();
  durationInSeconds = 5;
  private module: string[] = [];
  singlePackageData:any;
  TeacherModuleData:any=[];
  StudentModuleData:any=[];
  SchoolAdminData:any=[];
  
  constructor(private _sharedservice: SharedService, private packageservice:PackageService,private _location:Location , private router:Router, private formBuilder: FormBuilder, private modalService:NgbModal, private _snackBar: MatSnackBar) { 
    this.packageForm = formBuilder.group({
      name: ['', Validators.required],
      license_key: ['', Validators.required],
      price: ['', Validators.required],
      duration_in_month: ['', Validators.required],
      module: ['', Validators.required],
    })
  }


  ngOnInit(): void {
    this.get_single_package();
  }

  get_single_package(){
    let params = {
      page_size: this.pageSize,    

    }
    
    this.packageservice.GetAllPackage(params).subscribe(
      responce => {
        console.log(responce);
        if(responce.count > 0){
          this.allpackage = responce.results;
        }
      }
    )
  }

  module_insert(e: string) {

    const index: number = this.module.indexOf(e);
    if (index !== -1) {
      this.module.splice(index, 1);
    } else {
      this.module.push(e);
    }
    console.log(this.module);
  }
  
  editPackage(id:any){
    if (this.packageForm.valid) {
      let body = {
        name: this.packageForm.get('name')!.value,
        license_key : this.packageForm.get('license_key')!.value,
        price: this.packageForm.get('price')!.value,
        duration_in_month: this.packageForm.get('duration_in_month')!.value,
        modules:this.module
      }
    this.packageservice.UpdatePackage(id).subscribe(
      responce => {
        this._sharedservice.showMessage("Record Updated Successfully!" , "X",false);
        this.get_single_package();
      }
    );
  } else {
    this.markFormGroupTouched(this.packageForm);
  }
    setTimeout(() =>{ 
      this._sharedservice.showMessage("Record Updated Successfully!" , "X",false);
        this.get_single_package();
    }, 4000);
  }
  
  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: { markAsTouched: () => void; controls: any[]; })=> {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach((c: FormGroup) => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched);
  }
  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field)!.invalid && (form.get(field)!.dirty || form.get(field)!.touched),
      'is-valid': form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched)
    };
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

  goBack(): void {
    this._location.back();
  }

}
