import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from 'src/app/shared/service/dashboard.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  totalStudents:any;
  totalTeacher:any;
  Liststaffdetails:any;
  totalpackages:any;
  packagecount:any;
  schoolcount:any;

  constructor(private router:Router, private dashboard:DashboardService) { }

  
  data = null;


  ngOnInit(): void {

    this.getPackageData();
    this.getSchoolData();
    this.getStudentTeacherData();
    

  //       this.getWeather();
    
  //   setInterval(function(){ 
  //     var day = new Date().toLocaleString("en-US", {
  //       weekday: 'long'
  //   });
  //     var today = new Date().toLocaleString("en-US", {
        
  //       hour: '2-digit',
  //       minute: '2-digit',
  //       hour12: true
  //   });
  //   let location = document.getElementById("fday");
  //   location.innerHTML =day+', '+today;
  // }, 1000);
  }

  //  getWeather() {
	// 	let temperature = document.getElementById("temperature");
    
	// 	let api = "https://api.openweathermap.org/data/2.5/weather";
	// 	let apiKey = "f89d5dcfa990b82d0222ed62bfe80297";
	//  	navigator.geolocation.getCurrentPosition(success, error);
	// 	function success(position) {
  //  var latitude = position.coords.latitude;
  //  var longitude = position.coords.longitude;
	
	

  //   let url =
  //     api +
  //     "?lat=" +
  //     latitude +
  //     "&lon=" +
  //     longitude +
  //     "&appid=" +
  //     apiKey +
  //     "&units=imperial";

	
	
  //   fetch(url)
  //     .then(response => response.json())
  //     .then(data => {
  //       console.log(data);
  //       let temp = parseFloat(data.main.temp);
	// 	let tempinc=((temp-32)*5/9).toFixed(0);
  //       temperature.innerHTML = tempinc + "° C";
  //     });
     
  // }

  // function error() {
  //   //location.innerHTML = "Unable to retrieve your location";
  // }
	// }
 
 
  totalpackage(){
    this.router.navigate(['/packagemanagement']);
  }

  totalSchool(){
    this.router.navigate(['/schoolmanagement']);
  }

  
  getSchoolData(){
    var params = {
      page_size: '0'   
    }
    this.dashboard.getSchoolData(params).subscribe(data=>{
      console.log(data);
      this.Liststaffdetails=data.results.length;
      console.log(this.Liststaffdetails);
    })
  }

  getPackageData(){
    var params = {
      page_size: '0'   
    }
    this.dashboard.GetAllPackage(params).subscribe(data=>{
      console.log(data);
      this.totalpackages=data.length;
      console.log(this.totalpackages);
      
    })
  }

  getStudentTeacherData(){
    var params = {
      page_size: '0'   
    }
    this.dashboard.GetAllstudteach(params).subscribe(data=>{
      console.log(data);
      this.totalStudents=data.student_count;
      console.log(this.totalStudents);
      this.totalTeacher=data.teacher_count;
      console.log(this.totalTeacher);
      this.packagecount=data.package_count;
      console.log(this.packagecount);
      this.schoolcount=data.school_count;
      console.log(this.schoolcount);
    })
  }

}
