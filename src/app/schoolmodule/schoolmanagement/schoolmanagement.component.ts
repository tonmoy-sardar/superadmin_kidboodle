import { Location } from '@angular/common';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ngxCsv } from 'ngx-csv/ngx-csv';
import { itemPerPageNumList, msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition, page_size } from 'src/app/shared/global';
import { SchoolmanagementService } from 'src/app/shared/service/schoolmanagement.service';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-schoolmanagement',
  templateUrl: './schoolmanagement.component.html',
  styleUrls: ['./schoolmanagement.component.scss']
})
export class SchoolmanagementComponent implements OnInit {
  fieldTextType: boolean | undefined;


  @ViewChild('editModal') editModal: TemplateRef<any> | undefined  ;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  pageEvent!: PageEvent;
  // pageSize: string = page_size;
  pageSize:number = 10;
  pageSizeOption: Array<number> = itemPerPageNumList;
  pageNo: string = "1";
  page: number = 1;
  totalItems: number = 0;
  p: number = 1;
  userForm:any;
  SchoolList:any;
  school_type:any;
  SchoolListDropdown:any;
  schoolname: string = '';
  address1: string = '';
  address2: string = '';
  package: string = '';
  doj: string = '';
  reg_no: string = '';
  unique_id: string = '';
  pin_code: string = '';
  board: string = '';
  dial_code: string = '';
  contact_person_name: string = '';
  contact_person_phone: string = '';
  contact_person_email: string = '';
  school_phone1: string = '';
  school_phone2: string = '';
  school_email: string = '';
  account_holder: string = '';
  branch_name: string = '';
  bank_name: string = '';
  ifsc_code: string = '';
  account_number: string = '';
  is_active: boolean = true;
  attendence_type: string = '';
  country_code: string = '';
  state_code: string = '';
  city_code: string = '';
  username: string = '';
  password: string = '';
  schoollogo: any = '';
  singleSchoolData: any;
  schoolObj: any;
  SchoolValue: any = [];
  SchoolId:any;

  constructor(private _location:Location, private _snackBar: MatSnackBar, private modalService: NgbModal, private router:Router, private schoolManagement:SchoolmanagementService, private _sharedService:SharedService) { }

  ngOnInit(): void {
    console.log(localStorage.getItem("LoginToken"));
    var params = {
      page_size: 100,

    }
    this.schoolManagement.getSchoolData(params).subscribe((responce) => {
      console.log(responce);
      this.SchoolListDropdown = responce.results;
    })



    let msg = localStorage.getItem('msg');
    console.log(localStorage.getItem('msg'));
    if(msg != null && msg != ''){
      localStorage.setItem('msg','');
      this._sharedService.showMessage("One Record Added Successfully!", "X",true);
    }
    this.getData();
  }

  getData(){
    console.log(localStorage.getItem('isLoggedin'))

    var params = {
      page_size: 0

    }
    this.schoolManagement.getSchoolData(params).subscribe((responce) => {
      console.log(responce);
      this.totalItems = responce.count;
      this.SchoolList = responce.results;
    })
  }

  delButtonClick(id: any){
    var del = confirm("Do you want to delete?");
    if (del) {
      var body = {
          school_logo: this.schoollogo,
          data:{
          school_name: this.schoolname,
          address1: this.address1,
          address2: this.address2,
          package: this.package,
          doj: this.doj,
          reg_no: this.reg_no,
          unique_id: this.unique_id,
          pin_code: this.pin_code,
          board: this.board,
          dial_code: this.dial_code,
          contact_person_name: this.contact_person_name,
          contact_person_phone: this.contact_person_phone,
          contact_person_email: this.contact_person_email,
          school_phone1: this.school_phone1,
          school_phone2: this.school_phone2,
          school_email: this.school_email,
          account_holder: this.account_holder,
          branch_name: this.branch_name,
          bank_name: this.bank_name,
          ifsc_code: this.ifsc_code,
          account_number: this.account_number,
          is_active: this.is_active,
          attendence_type: this.attendence_type,
          country_code: this.country_code,
          state_code: this.state_code,
          city_code: this.city_code,
          username: this.username,
          password: this.password 
          }
          }
      this._sharedService.showMessage("One Record Deleted Successfully!", "X",false);
      this.schoolManagement.deleteSchoolData(id,body).subscribe((responce) => {
        console.log(responce);
       this.getData();
      })
    }
  }

  addSchool() {
    this.router.navigate(['/addnewschool']);
  }

  viewSchool(id:any) {
    // this.router.navigate(['/viewschooldetails']);
    this.modalService.open(this.editModal, { size: 'lg' });
    let studentData = this.SchoolList.find((e: { id: any; }) =>e.id == id);
    if(studentData !== null){
      this.singleSchoolData = studentData;
      console.log(this.singleSchoolData);
    }
    // this.singleStudentFeeMonth = this.monthList.find(e => e.id === +this.month);
    // console.log(this.singleStudentFeeMonth);
    // this.feeManagement(this.singleStudentData?.scl_class);

  }

  FormDownload(){


    for (const item of this.SchoolList){
      var obj = {
        school_name: item.school_name,
        address1: item.address1,
        contact_person_name:item.contact_person_name,
        contact_person_email:item.contact_person_email,
        contact_person_phone:item.contact_person_phone,
        doj:item.doj,
        package_name:item.package_name,
        school_user_username:item.school_user_username,

      }
      this.SchoolValue.push(obj);
    }


    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: false,
      title: 'SchoolList',
      useBom: true,
      headers: ["School Name", "Address", "Contact Person Name","Contact Person Email","Contact Person Phone","Date of Registration","Package Name","Username"]
    };
   
    new ngxCsv(this.SchoolValue,"SchoolList", options);
  }

  onSearch(){
    var params = {
      page_size: this.pageSize,
      search: this.schoolname

    }
    this.schoolManagement.getSchoolData(params).subscribe((responce) => {
      console.log(responce);
      this.SchoolList = responce.results;
    })
  }

  ActiveClick(id:any,is_active:boolean){
    var del = confirm("Do You want to Change the Status?");
    if (del) {
  if(is_active==true){
    var params={
      school_id:id,
      method:'edit',
      is_active:'0'
    }
  }
  else{
    var params={
      school_id:id,
      method:'edit',
      is_active:'1'
    }
  }
  this.schoolManagement.updateSchoolStatus(params).subscribe(data=>{
    this._sharedService.showMessage('Status Changed Successfully', "X", true);

    console.log(data);
    this.getData();
  })
}
  }

  // ActiveClick(id:any,is_active:boolean){
    
  //     var params={
  //       school_id:id,
  //       method:'edit',
  //       is_active:'0'
  //   }
  //   this.schoolManagement.updateSchoolStatus(params).subscribe(data=>{
  //     console.log(data);
  //     this.getData();
  //   })
  //   }

  editSchool(id: any){
    localStorage.setItem("id", id)
    this.router.navigate(['/editschooldetails']);
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

  displayStyle = "none";
  closePopup() {
    this.displayStyle = "none";
  }

  goBack(): void {
    this._location.back();
  }
  
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }

  loginForm(id:any,school_user_username: any){
  this.SchoolId=school_user_username;
    console.log(id);
    console.log(school_user_username);

    window.open("http://schooladmin.kidboodle.com/#/admin_login/" +this.SchoolId,"DescriptiveWindowName",
"left=100,top=100,width=1390,height=760")

   
  }

  // onChangePage(pe: PageEvent) {
  //   this.pageSize = pe.pageSize.toString();
  //   this.pageNo = (pe.pageIndex + 1).toString();
  //   this.getData()
  // }

  handlePageChange(event:any){ 
    this.page = event;
}

}
