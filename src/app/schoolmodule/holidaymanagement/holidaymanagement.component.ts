import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatSnackBar, MatSnackBarRef } from '@angular/material/snack-bar';
import { itemPerPageNumList, msgduration, msghorizontalPosition, msgpanelClass, msgverticalPosition, page_size } from 'src/app/shared/global';
import { SharedService } from 'src/app/shared/shared.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HolidaylistService } from 'src/app/shared/service/holidaylist.service';
import { HttpParams } from '@angular/common/http';
import { ngxCsv } from 'ngx-csv/ngx-csv';

@Component({
  selector: 'app-holidaymanagement',
  templateUrl: './holidaymanagement.component.html',
  styleUrls: ['./holidaymanagement.component.scss']
})
export class HolidaymanagementComponent implements OnInit {

  sub: Subscription = new Subscription();
  itemList: any;
  HolidayList: any = [];

  @ViewChild('editModal') editModal: TemplateRef<any> | undefined;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  pageEvent!: PageEvent;
  // pageSize: string = page_size;
  pageSizeOption: Array<number> = itemPerPageNumList;
  pageNo: string = "1";
  totalItems: number = 0;
  page: number = 1;
  pageSize:number = 10;
  p: number = 1;
  holidayData: any = [];


  public form!: FormGroup;
  editid: any;
  holidayForm!: FormGroup;
  holidayObj: any;
  leavetype: string = '';
  icon_image: any = '';
  editleaveicon: any = '';
  editleavetype: string = '';
  filen:string ='Choose File';

  imagepath:any;
  constructor(private formBuilder: FormBuilder, private _sharedService: SharedService, private router: Router, private _snackBar: MatSnackBar, private _location: Location,
    private modalService: NgbModal,
    private holidayListService: HolidaylistService, private fb: FormBuilder
  ) {
    this.holidayForm = formBuilder.group({
      leavetype: ['', Validators.required],
      icon_image: [null],
    })
  }

  ngOnInit(): void {

    this.getDetails();
    let msg = localStorage.getItem('msg');
    if(msg != "" && msg != null){
      this._sharedService.showMessage(msg, "X",true);
      localStorage.setItem("msg","");
    }
    // console.log(localStorage.getItem("LoginToken"));
   
  }


  getDetails() {
    var params = {
      page_size: 100

    }
    this.holidayListService.getHolidayData(params).subscribe((responce) => {
      console.log(responce);
      this.totalItems = responce.length;
      this.HolidayList = responce.results;
    })
  }


  selectFile(event:any,image:any) {
   
    if (event.target.files[0].type == "image/png" || event.target.files[0].type == "image/jpeg" || event.target.files[0].type == "image/jpg") {
      if (event.target.files && event.target.files[0]) {
        
        this.holidayForm.patchValue({
          icon_image: event.target.files[0]
          })
          this.filen = event.target.files[0].name;
      }
    } else {

      this.icon_image = "Choose file";
      alert("Please select png / jpeg / jpg");
    }
  }

  addDetails() {
    var formValue = new FormData();
    formValue.append('icon_image', this.holidayForm.value.icon_image);
    if (this.holidayForm.value.icon_image){
      formValue.append('icon_image', this.holidayForm.value.icon_image, this.holidayForm.value.icon_image['name']);
    }
    else{
        formValue.append('icon_image', '');
    };
    formValue.append('leave_type', this.holidayForm.value.leavetype);
    if (this.holidayForm.valid) {
    var body = {
      leave_type: this.leavetype,
      // icon_image: this.icon_image
    }
    

    this.holidayListService.addHolidayData(formValue).subscribe((response) => {
      localStorage.setItem('msg','One Record Added Successfully');
      this.getDetails();
      console.log(response);
      if(response.id !=""){
        location.reload()
      }
      
    }, err => {
      if(err.status==400){
        this._sharedService.showMessage("Username Already Exists!", "X",false);
      }
    }
    );
  }
  else {

    this.markFormGroupTouched(this.holidayForm);
    // this._dataService.markFormGroupTouched(this.userForm);
  }
}

  updateDetails() {
  //   var formValue = new FormData();
  //   formValue.append('icon_image', this.holidayForm.value.icon_image);
  //   if (this.holidayForm.value.icon_image){
  //     formValue.append('icon_image', this.holidayForm.value.icon_image, this.holidayForm.value.icon_image['name']);
  //   }
  //   else{
  //       formValue.append('icon_image', '');
  //   };
  //   formValue.append('leave_type', this.holidayForm.value.leavetype);
  //   if (this.holidayForm.valid) {
  //   var body = {
  //     leave_type: this.leavetype,
  //   }
    

  //   this.holidayListService.updateHolidayData(id,formValue).subscribe((response) => {
  //     localStorage.setItem('msg','one Record Added Successfully');
  //     this.getDetails();
  //     console.log(response);
  //     if(response.id !=""){
  //       location.reload()
  //     }
      
  //   }, err => {
  //     if(err.status==400){
  //       this._sharedService.showMessage("Username Already Exists!", "X",false);
  //     }
  //   }
  //   );
  // }
  // else {

  //   this.markFormGroupTouched(this.holidayForm);
  //   // this._dataService.markFormGroupTouched(this.userForm);
  // }
  }

  onDelete(id: any) {
    this.router.navigate(['/holidaymanagement']);
    var del = confirm("Do you want to delete?");
    if (del) {
      
      this.holidayListService.deleteHolidayData(id).subscribe((responce) => {
        
        
      })
      localStorage.setItem('msg','One Record Deleted Successfully!');
        setTimeout(function(){ location.reload() }, 4000);
      
    }
  }

  FormDownload(){


    for (const item of this.HolidayList){
      var obj = {
        leave_type: item.leave_type,
        icon_image_url:item.icon_image_url,
      }
      this.holidayData.push(obj);
    }


    var options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalseparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'HolidayList',
      useBom: true,
      headers: ["leave_type", "icon_image_url"]
    };
   
    new ngxCsv(this.holidayData,"HolidayList", options);
  }

  showMessage(message: string) {
    this._snackBar.open(message, "x", {
      duration: msgduration,
      verticalPosition: msgverticalPosition,
      horizontalPosition: msghorizontalPosition,
      panelClass: msgpanelClass,
    });
  }

  markFormGroupTouched(formGroup: FormGroup) {
    (<any>Object).values(formGroup.controls).forEach((control: { markAsTouched: () => void; controls: any[]; })=> {
      control.markAsTouched();
      if (control.controls) {
        control.controls.forEach((c: FormGroup) => this.markFormGroupTouched(c));
      }
    });
  }

  isFieldValid(form: FormGroup, field: string) {
    return !form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched);
  }
  displayFieldCss(form: FormGroup, field: string) {
    return {
      'is-invalid': form.get(field)!.invalid && (form.get(field)!.dirty || form.get(field)!.touched),
      'is-valid': form.get(field)!.valid && (form.get(field)!.dirty || form.get(field)!.touched)
    };
  }

  // onChangePage(pe: PageEvent) {
  //   this.pageSize = pe.pageSize.toString();
  //   this.pageNo = (pe.pageIndex + 1).toString();
  //   this.getDetails();
  // }

  openModel(id: any) {
    this.editid = id;
    console.log(this.editid);
    this.modalService.open(this.editModal, { size: 'lg' });
  }

  displayStyle = "none";
  closePopup() {
    this.displayStyle = "none";
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }

  goBack(): void {
    this._location.back();
  }

  handlePageChange(event:any){ 
    this.page = event;
}

}
