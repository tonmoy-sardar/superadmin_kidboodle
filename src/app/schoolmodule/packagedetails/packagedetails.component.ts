import { Component, OnInit } from '@angular/core';
import {Location} from '@angular/common';
import { PackageService } from '../../shared/service/package.service';
import { page_size } from 'src/app/shared/global';

@Component({
  selector: 'app-packagedetails',
  templateUrl: './packagedetails.component.html',
  styleUrls: ['./packagedetails.component.scss']
})
export class PackagedetailsComponent implements OnInit {

  pageSize: string = page_size;
  allpackage:any;

  
  constructor( private packageservice:PackageService, private _location:Location) { }

  ngOnInit(): void {

    this.get_package();

    // let id = localStorage.getItem("package_id");
    // console.log(id);
    // this.packageservice.Packagedetails(id).subscribe(
    //   responce => {
    //     console.log(responce);
        
    //     // var optionList: Array<AppOption> = new Array<AppOption>();
    //     // this.allSubjectList.filter(e => e.isSelected == false);
    //     // if (this.ClassValue != "")
    //     //   responce.results.forEach(element => {
           
    //     //     this.allSubjectList.find(e => e.DisplayName == element.subject).isSelected = true;
    //     //     // this.allSubjectList.find(e => e.DisplayName == element.subject).id = element.id;
    //     //   });
    //     // this.subjectList = optionList;
    //   }
    // )
  }


  get_package(){
    let params = {
      page_size: this.pageSize,    
      id:localStorage.getItem('package_id')
    }
    
    this.packageservice.GetAllPackage(params).subscribe(
      responce => {
        console.log(responce);
        // if(responce.count > 0){
          this.allpackage = responce.results;
          console.log(this.allpackage);
        }
      // }
    )
  }


  goBack(): void {
    this._location.back();
  }

}
