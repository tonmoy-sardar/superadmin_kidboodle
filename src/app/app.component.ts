import { Component,OnInit } from '@angular/core';
import { Location } from '@angular/common';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'superadmin';
  showFiller = false;
  constructor(private location: Location) {
    
   }
       
       ngOnInit() {
         
          //  if (location.protocol === 'http:') {
          //    window.location.href = location.href.replace('http', 'https');
          //  }
         this.HeaderVisible();
       }
       HeaderVisible(){
  
        if( localStorage.getItem('isLoggedin'))
         {
           this.showFiller==true;
             }
           else{ this.showFiller==false; }
      }
   
}
