import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav';
import { NavigationStart, Router } from '@angular/router';
import { LoginClass } from 'src/app/shared/schoolclass/loginpage.class';
import { LoginService } from 'src/app/shared/service/login.service';
import { SharedService } from 'src/app/shared/shared.service';
import { Subscription } from 'rxjs';
import { Changepswd } from 'src/app/shared/schoolclass/changepswd';
import { FormGroup } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { MyprofileService } from 'src/app/shared/service/myprofile.service';
import { ModuleService } from 'src/app/shared/service/module.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @ViewChild('editModal') editModal: TemplateRef<any> | undefined;

  // currentSession: string = localStorage.getItem("CurrentSession");
  sub: Subscription = new Subscription();

  showHeader: boolean = false;
  userName: any;
  SchoolName:any;

  @Input()
  draw!: MatDrawer;

  public misMatchError: boolean = false;
  public userData = new Changepswd();
  userForm!: FormGroup;

  
  constructor(private _sharedService: SharedService, private _router: Router, private loginService: LoginService, private modalService: NgbModal,
    private _myprofileService: MyprofileService,private _module:ModuleService

    ) { }

  togle() {
    this.draw.toggle();
  }

  ngOnInit(): void {
    this.showHeader=false;
    this._router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        if ("loginpage,forgetpassword".indexOf(location.pathname.split('/')[1]) > 0) {
          this.showHeader = false;
        } else{
          this.showHeader=true;
        }
      }
    }
    );
    // this.UserImage=localStorage.getItem("Image");
    if(localStorage.getItem("isLoggedin") != undefined ){
      this.showHeader=true;
    } else {
      this.showHeader=false;
    }
    
  }

  menu_click(menuname: string) {
    switch (menuname) {
     
      case 'My Account':
        this._router.navigate(['/myprofile']);
        break;
      case 'Sign Out':
        
        localStorage.clear();
        // this._router.navigate(['/login']);
        this._router.navigate(['/login'])
         .then(() => {
         window.location.reload();
  });

        break;
        
      default:
        this._router.navigate(['/dashboard']);
    }
  }

  openNav() {
    (<HTMLInputElement>document.getElementById("mySidepanel")).style.width = "250px";
  }

  changePaswd(){

    if(this.userData.newpassword == "" && this.userData.newpasswordcopy==""){
      this._sharedService.showMessage("Please Enter Require Fields", "X",false);
    }
    else{
    if (this.userData.newpassword != this.userData.newpasswordcopy) {
      this._sharedService.showMessage("new password and confirm password not match", "X",false);
    }
    else{
    var body={
      username:localStorage.getItem("email_id"),
      password:this.userData.newpasswordcopy
    }
    var id=localStorage.getItem("school_user_id");

    this._module.changePassword(id,body).subscribe(data=>{
      console.log(data);
      this._sharedService.showMessage("Password Changed Successfully!", "X", true);
      this.modalService.dismissAll();
    })
  }
}
}

  submit() {
    if (this.userData.oldpassword == this.userData.newpassword) {
      this._sharedService.showMessage("new password and old password cannot be identical", "X",false);

      return;
    }

    if (this.userData.newpassword != this.userData.newpasswordcopy) {
      this._sharedService.showMessage("new password and confirm password not match", "X",false);
      return
    }
    this._myprofileService.getMyprofileData(this.userData.oldpassword, this.userData.newpassword).subscribe(responce => {

      if (responce.request_status == "1") {
        this._sharedService.showMessage("Password changed successfully! Please Login again.","X",true);
        setTimeout(() => {
          this._sharedService.navigateToroute('login');
        }, 600);
      }
      else {
        this._sharedService.showMessage(responce.results.msg, "X",false);
      }
    },
      (error) => {
        this._sharedService.showMessage(error.results.msg, "X",false)

      })
  }
  
  openModel() {
    this.modalService.open(this.editModal, { size: 'lg' });
  }

}
