import { BreakpointObserver } from '@angular/cdk/layout';
import { Component, Input, OnInit } from '@angular/core';
import { MatDrawer } from '@angular/material/sidenav'; 
import { NavigationStart, Router } from '@angular/router';
import { SharedService } from 'src/app/shared/shared.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  showHeader: boolean = false;
  // username:string;
  @Input()
  draw!:MatDrawer;
  UserImage:any;

  constructor(private breakpointObserver: BreakpointObserver,private _router: Router, private _sharedService: SharedService) { 
    // this.username=localStorage.getItem("username");

    this._router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        let pageTitle:string= location.pathname.split('/')[1]; 
        if ("loginpage,forgetpassword".indexOf(pageTitle) > 0) {
          this.showHeader = false;
        } else{
          this.showHeader=true; 
        }
      }
    }
    );
  }

  

  ngOnInit(): void {
    // this.username=localStorage.getItem("username");
    this.UserImage=localStorage.getItem("Image");

  }

  menu_click(menuname: string) {
    this._sharedService.navigateToroute(menuname);
   
     setTimeout(() => {
         this.draw.toggle(); 
         this._sharedService.setSubHeader();  
     }, 100);
   }

   openNav() {
    (<HTMLInputElement>document.getElementById("mySidepanel")).style.width = "250px";
  }

  closeNav(){
    (<HTMLInputElement>document.getElementById("mySidepanel")).style.width = "0px";
  }

  closesidebar(){
   this.closeNav();
  }
  
  
  }