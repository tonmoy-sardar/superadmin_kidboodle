import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SchoolmenuComponent } from './schoolmenu.component';

describe('SchoolmenuComponent', () => {
  let component: SchoolmenuComponent;
  let fixture: ComponentFixture<SchoolmenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SchoolmenuComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SchoolmenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
