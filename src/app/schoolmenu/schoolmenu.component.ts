import { Component, OnInit, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SharedService } from '../shared/shared.service';
import { MatDrawer } from '@angular/material/sidenav';

@Component({
  selector: 'app-schoolmenu',
  templateUrl: './schoolmenu.component.html',
  styleUrls: ['./schoolmenu.component.scss']
})
export class SchoolmenuComponent implements OnInit {

  showFiller:boolean=false;
  showHeader: boolean=true;
  @ViewChild('drawer', { static: false }) public drawer!: MatDrawer;

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private _sharedService: SharedService, private router: Router) { }

  toggle(){
    
    this.drawer.toggle()
      }
      

  ngOnInit(): void {
    this.HeaderVisible();
    this._sharedService.showHeaderSubject.subscribe((responce)=> {
      this.showHeader=responce as boolean
  });
  }

  HeaderVisible(){
  console.log(localStorage.getItem('isLoggedin'));
    if( localStorage.getItem('isLoggedin'))
     {
       this.showFiller=true;
         }
       else{
       this.showFiller=false;
         }
       }
   
 

}
