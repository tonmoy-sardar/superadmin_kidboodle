import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTreeModule } from '@angular/material/tree';
import { HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatPaginatorModule} from '@angular/material/paginator';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InterceptorService } from './shared/service/sharedservice/interceptor.service';
import { LoginpageComponent } from './login/loginpage/loginpage.component';
import { ForgotpasswordComponent } from './login/forgotpassword/forgotpassword.component';
import { DashboardComponent } from './schoolmodule/dashboard/dashboard.component';
import { SchoolmanagementComponent } from './schoolmodule/schoolmanagement/schoolmanagement.component';
import { PackagemanagementComponent } from './schoolmodule/packagemanagement/packagemanagement.component';
import { ModulemanagementComponent } from './schoolmodule/modulemanagement/modulemanagement.component';
import { HeaderComponent } from './schoolmenu/header/header.component';
import { SidebarComponent } from './schoolmenu/sidebar/sidebar.component';
import { LinkComponent } from './schoolmenu/link/link.component';
import { SchoolmenuComponent } from './schoolmenu/schoolmenu.component';
import { FooterComponent } from './schoolmenu/footer/footer.component';
import { HolidaymanagementComponent } from './schoolmodule/holidaymanagement/holidaymanagement.component';
import { MatDividerModule } from '@angular/material/divider';
import { AddnewschoolComponent } from './schoolmodule/addnewschool/addnewschool.component';
import { PackagedetailsComponent } from './schoolmodule/packagedetails/packagedetails.component';
import { AddpackageComponent } from './schoolmodule/addpackage/addpackage.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { EditschooldetailsComponent } from './schoolmodule/editschooldetails/editschooldetails.component';
import { ViewschooldetailsComponent } from './schoolmodule/viewschooldetails/viewschooldetails.component';
import { EditpackagedetailsComponent } from './schoolmodule/editpackagedetails/editpackagedetails.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginpageComponent,
    ForgotpasswordComponent,
    DashboardComponent,
    HolidaymanagementComponent,
    SchoolmanagementComponent,
    PackagemanagementComponent,
    ModulemanagementComponent,
    HeaderComponent,
    SidebarComponent,
    SchoolmenuComponent,
    LinkComponent,
    FooterComponent,
    AddnewschoolComponent,
    PackagedetailsComponent,
    AddpackageComponent,
    EditschooldetailsComponent,
    ViewschooldetailsComponent,
    EditpackagedetailsComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatDividerModule,
    MatListModule,
    MatTreeModule,
    HttpClientModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    MatSnackBarModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule,
    NgbModule,
    NgxPaginationModule,MatSnackBarModule,


  ],
  providers: [ { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    {provide: LocationStrategy, useClass: HashLocationStrategy}],  bootstrap: [AppComponent]
})
export class AppModule { }
