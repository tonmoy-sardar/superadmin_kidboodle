import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/auth.guard';
import { ForgotpasswordComponent } from './login/forgotpassword/forgotpassword.component';
import { LoginpageComponent } from './login/loginpage/loginpage.component';
import { AddnewschoolComponent } from './schoolmodule/addnewschool/addnewschool.component';
import { AddpackageComponent } from './schoolmodule/addpackage/addpackage.component';
import { DashboardComponent } from './schoolmodule/dashboard/dashboard.component';
import { EditpackagedetailsComponent } from './schoolmodule/editpackagedetails/editpackagedetails.component';
import { EditschooldetailsComponent } from './schoolmodule/editschooldetails/editschooldetails.component';
import { HolidaymanagementComponent } from './schoolmodule/holidaymanagement/holidaymanagement.component';
import { ModulemanagementComponent } from './schoolmodule/modulemanagement/modulemanagement.component';
import { PackagedetailsComponent } from './schoolmodule/packagedetails/packagedetails.component';
import { PackagemanagementComponent } from './schoolmodule/packagemanagement/packagemanagement.component';
import { SchoolmanagementComponent } from './schoolmodule/schoolmanagement/schoolmanagement.component';
import { ViewschooldetailsComponent } from './schoolmodule/viewschooldetails/viewschooldetails.component';


const routes: Routes = [
  {path:'login', component:LoginpageComponent},
  {path:'dashboard', component:DashboardComponent, canActivate: [AuthGuard]},
  {path:'forgotpassword', component:ForgotpasswordComponent},
  {path:'holidaymanagement', component:HolidaymanagementComponent,canActivate: [AuthGuard]},
  {path:'packagemanagement', component:PackagemanagementComponent,canActivate: [AuthGuard]},
  {path:'modulemanagement', component:ModulemanagementComponent,canActivate: [AuthGuard]},
  {path:'schoolmanagement', component:SchoolmanagementComponent,canActivate: [AuthGuard]},
  {path:'addnewschool', component:AddnewschoolComponent,canActivate: [AuthGuard]},
  {path:'addpackage', component:AddpackageComponent,canActivate: [AuthGuard]},
  {path:'packagedetails', component:PackagedetailsComponent,canActivate: [AuthGuard]},
  {path:'editpackagedetails', component:EditpackagedetailsComponent,canActivate: [AuthGuard]},
  {path:'editschooldetails', component:EditschooldetailsComponent,canActivate: [AuthGuard]},
  {path:'viewschooldetails', component:ViewschooldetailsComponent,canActivate: [AuthGuard]},


  {path:'*', component:LoginpageComponent},
  {path:'**', component:LoginpageComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { 
  useHash: true })],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }
