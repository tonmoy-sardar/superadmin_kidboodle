export const environment = {
  production: true,
  // apiURL:"http://3.17.248.213:8002/",
  apiURL:"https://kidboodle.com/api/",
  appTitle:"Kidboodle",
  page_size:"5",
  pageSizeOptions:"[5, 10, 20]",
  globalCurrency:'₹'
};
